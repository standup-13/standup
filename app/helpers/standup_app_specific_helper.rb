

module StandupAppSpecificHelper

	# Prepare @proj_ids and @user_ids lists

	def __app_prep_table

		tasktracker_project = Project.where("identifier='tasktracker'").first
		tracked_projects =
			[tasktracker_project] +
			Project.where("parent_id = ?", tasktracker_project.id)
		proj_info = {}
		for i in tracked_projects do
			proj_info[i.id] = {
				name: i.name
			}
		end
		@proj_ids = proj_info.keys

		groups = Group.where("lastname='TaskTracker'")
		group = groups.first
		@user_ids = group.user_ids

	end

	# Color styles for projects

	@__unknown_style  = "color:black; background-color:white  ; border-color:black"
	@__overhead_style = "color:black; background-color:#cccccc; border-color:black"
	@__ax_style       = "color:black; background-color:#a6caf0; border-color:black"
	@__x2_style       = "color:black; background-color:#ffff99; border-color:black"
	@__g280_style     = "color:black; background-color:#ff9999; border-color:black"
	@__refresh_style  = "color:black; background-color:#99ffcc; border-color:black"
	@__new_swm_style  = "color:black; background-color:#d0b0ff; border-color:black"
	@__proj_styles = {
		"" => {
			"" => @__unknown_style,
		},
		"cz-private" => {
			"" => @__overhead_style,
		},
		"tasktracker" => {
			"" => @__overhead_style,
			"AX only" => @__ax_style,
			"X2 only" => @__x2_style,
		},
		"ax" => {
			"" => @__ax_style,
		},
		"x2" => {
			"" => @__x2_style,
		},
		"g280" => {
			"" => @__g280_style,
		},
		"refresh" => {
			"" => @__refresh_style,
		},
		"new-software-master" => {
			"" => @__new_swm_style,
		}
	}

	def self.__proj_styles
		@__proj_styles
	end

end

