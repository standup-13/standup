module StandupHelper
	include ContextMenusHelper

	def render_box(user, issue, date, weekend)
		if date then
			start_date = issue.start_date
			end_date = issue.end_date
			if (
				(start_date and start_date > date) or
				(end_date and end_date < date)
			) then
				return ""
			end
		end
		
		issue_start = (!date or date == start_date)
		issue_end = (!date or date == end_date)

		pending = (issue.is_pending or weekend)
		unfinished = issue.done_ratio < 100
		
		common_style =
			"box-sizing: border-box; width:100%; " +
			"border-width:0.1em; border-style:solid; " +
			"padding:0.2em; line-height:1em; white-space:nowrap; " +
			"overflow:hidden; text-overflow:ellipsis; "

		extra_style =
			(issue_start ? "border-left-width:0.5em; " : "") +
			(issue_end ? "border-right-width:0.5em; ": "") +
			(pending ? "opacity:0.25; " : "")

		orig_style = common_style + extra_style + issue.style

		return (
"<div
	style='padding:0.0625em'
	class='draggable'

	user_id='#{user.id}'
	issue_id='#{issue.id}'
	date='#{date ? date.strftime("%Y-%m-%d") : ""}'
	is_start='#{issue_start ? "1" : ""}'
	is_end='#{issue_end ? "1" : ""}'
>
	<div
		name='issue#{issue.id}'
		class='issue_div'

		issue_id='#{issue.id}'

		orig_style='#{orig_style}'
		style='#{orig_style}'

		title='
			<b>#{ issue.tracker } ##{ issue.id }:</b> #{ CGI.escapeHTML(issue.subject) }<br>
			<br>
			<b>Status:</b> #{ issue.status }<br>
			<b>Done:</b> #{ issue.done_ratio }%<br>
			<br>
			<b>Project:</b> #{ CGI.escapeHTML(issue.project.name) }<br>
			<b>Category:</b> #{ issue.category }<br>
			<b>Start date:</b> #{ issue.start_date_format('%a %Y-%m-%d', 'unknown') }<br>
			<b>Due date:</b> #{ issue.due_date_format('%a %Y-%m-%d', 'unknown') }<br>
			#{ issue.is_closed ? "<b>Closed date:</b> #{ issue.closed_date_format('%a %Y-%m-%d', 'unknown') }<br>" : "" }'
	>
		".html_safe + issue.subject + "
	</div>
</div>".html_safe)

	end
end
