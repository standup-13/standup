
class StandupController < ApplicationController

	def index
		@can_add = User.current.allowed_to?(:add_issues, @project)
		@can_edit = User.current.allowed_to?(:edit_issues, @project)
		__prep_table
		nil
	end

	class Issue_wrapper
		@vac_trackers = []
		for i in Tracker.where("name IN ('VAC')") do
			@vac_trackers.push(i.id)
		end
		@new_states = []
		for i in IssueStatus.where("name IN ('New', 'Pending')") do
			@new_states.push(i.id)
		end
		@closed_states = []
		for i in IssueStatus.where("is_closed = true") do
			@closed_states.push(i.id)
		end
		@pending_states = []
		for i in IssueStatus.where("name IN ('Pending', 'Feedback')") do
			@pending_states.push(i.id)
		end

		def initialize(db_entry)
			@db_entry = db_entry

			@end_date = due_date
			if is_closed then
				c = closed_date
				if not @end_date or c < @end_date then
					s = start_date
					if s and s > c then
						@end_date = s
					else
						@end_date = c
					end
				end
			end
		end

		def format_date(date, format, default)
			if date then
				return date.strftime(format)
			else
				return default
			end
		end

		def start_date
			@db_entry.start_date
		end
		def start_date_format(format, default)
			format_date(start_date, format, default)
		end

		def due_date
			@db_entry.due_date
		end
		def due_date_format(format, default)
			format_date(due_date, format, default)
		end

		def closed_date
			@db_entry.closed_on.to_date
		end
		def closed_date_format(format, default)
			format_date(closed_date, format, default)
		end

		def end_date
			@end_date
		end

		def status
			@db_entry.status
		end
		def tracker
			@db_entry.tracker
		end
		def project
			@db_entry.project
		end
		def subject
			@db_entry.subject
		end
		def id
			@db_entry.id
		end
		def done_ratio
			@db_entry.done_ratio
		end
		def category
			@db_entry.category
		end

		def self.pending_states
			@pending_states
		end
		def is_pending
			return Issue_wrapper.pending_states.include? @db_entry.status_id
		end

		def self.closed_states
			@closed_states
		end
		def is_closed
			return Issue_wrapper.closed_states.include? @db_entry.status_id
		end

		def intersect_with_range(b2, e2)
			b1 = start_date
			e1 = end_date
			return (
				(not b1 or not e2 or b1 <= e2) and
				(not b2 or not e1 or b2 <= e1)
			)
		end

		def intersect_with_issue(i)
			return intersect_with_range(i.start_date, i.end_date)
		end

		def intersect_with_group(g)
			for i in g do
				if intersect_with_issue(i) then
					return true
				end
			end
			return false
		end

		def style
			proj_id = @db_entry.project_id
			cat_id = @db_entry.category_id

			proj_styles = StandupAppSpecificHelper.__proj_styles

			if proj_styles.key? proj_id then
				proj = proj_styles[proj_id]
			else
				identifier = Project.find(proj_id).identifier
				if proj_styles.key? identifier then
					proj = proj_styles[identifier]
					proj_styles[proj_id] = proj
				else
					proj = proj_styles[""]
				end
			end

			if cat_id == nil then
				cat_id = ""
			end

			if proj.key? cat_id then
				cat = proj[cat_id]
			else
				name = IssueCategory.find(cat_id).name
				if proj.key? name then
					cat = proj[name]
					proj[cat_id] = cat
				else
					cat = proj[""]
				end
			end

			return cat
		end

	end

	include StandupAppSpecificHelper

	class Empty_filter
		def initialize
			nil
		end
		def match?(str)
			true
		end
	end

	class Word_filter
		def initialize(w)
			@w = w.downcase
			nil
		end
		def match?(str)
			str.downcase.include? @w
		end
	end

	def __prep_table

		# Prepare @proj_ids and @user_ids lists
		__app_prep_table

		@users = User.order(:lastname, :firstname, :id).find(@user_ids)

		begin
			@first_day = Date.strptime(params[:table_start_date], "%Y-%m-%d")
		rescue
			@first_day = Date.today
		end

		begin
			@table_filter = Word_filter.new params[:table_filter]
		rescue
			@table_filter = Empty_filter.new
		end

		@last_day = @first_day + 7
		distant_future = @first_day + 14

		@issues = Issue.where(
			"project_id IN (?) and " +
			"assigned_to_id IN (?) and " +
			"status_id NOT IN (?)",
			@proj_ids,
			@user_ids,
			Issue_wrapper.closed_states
		) + Issue.where(
			"project_id IN (?) and " +
			"assigned_to_id IN (?) and " +
			"status_id IN (?) and " +
			"((start_date is null) or (start_date <= '#{@last_day.strftime("%Y%m%d")}')) and " +
			"((closed_on is null) or ('#{@first_day.strftime("%Y%m%d")}' <= closed_on))",
			@proj_ids,
			@user_ids,
			Issue_wrapper.closed_states
		)
		@issues_by_user = {}
		for i in @users do
			@issues_by_user[i.id] = []
		end
		for i in @issues do
			@issues_by_user[i.assigned_to_id].push(Issue_wrapper.new i)
		end

		@show_user = {}

		for user in @users do
			tasks = @issues_by_user[user.id].sort_by do | u |
				[
					u.tracker.name,
					u.project.name,
					u.subject,
					u.id
				]
			end

			show_user = ( (@table_filter.match? user.firstname) or (@table_filter.match? user.lastname) )
			for task in tasks do
				if not show_user and (@table_filter.match? task.subject) then
					show_user = true
				end
			end
			@show_user[user.id] = show_user
			if not show_user then
				next
			end

			past = []
			present = []
			future = []

			@issues_by_user[user.id] = {
				"past" => past,
				"present" => present,
				"future" => future,
			}
			for task in tasks do
				end_date = task.end_date
				if end_date and end_date < @first_day then
					past.push(task)
					next
				elsif task.start_date and task.start_date > @last_day then
					future.push(task)
					next
				end

				merged = false
				for g in present do
					if not task.intersect_with_group(g) then
						g.push(task)
						merged = true
						break
					end
				end

				if not merged then
					present.push([task])
				end
			end
		end
		nil
	end

	def table
		__prep_table
		render :layout => false
		nil
	end

	def set_status
		@issues = Issue.where(:id => params[:issue_id])
		@issue = @issues.first

		@project = Project.find(@issue.project_id)
		@can_edit = User.current.allowed_to?(:edit_issues, @project)
		if @can_edit then
			journal = @issue.init_journal(User.current, "Changed by 'standup' plugin.")
			@issue.safe_attributes = {"status_id" => params[:status]}
			ok = @issue.save
		end
	rescue
	ensure
		__prep_table
		render :action => "table", :layout => false
		nil
	end

	def set_assignee
		@issues = Issue.where(:id => params[:issue_id])
		@issue = @issues.first

		@project = Project.find(@issue.project_id)
		@can_edit = User.current.allowed_to?(:edit_issues, @project)
		assignee = params[:assignee]
		assignable = true # TODO: project.assignable_users.include? assignee
		if @can_edit and assignable then
			journal = @issue.init_journal(User.current, "Changed by 'standup' plugin.")
			@issue.safe_attributes = {"assigned_to_id" => assignee}
			ok = @issue.save
		end
	rescue
	ensure
		__prep_table
		render :action => "table", :layout => false
		nil
	end

	def set_done_ratio
		@issues = Issue.where(:id => params[:issue_id])
		@issue = @issues.first

		@project = Project.find(@issue.project_id)
		@can_edit = User.current.allowed_to?(:edit_issues, @project)
		if @can_edit then
			journal = @issue.init_journal(User.current, "Changed by 'standup' plugin.")
			@issue.safe_attributes = {"done_ratio" => params[:done_ratio]}
			ok = @issue.save
		end
	rescue
	ensure
		__prep_table
		render :action => "table", :layout => false
		nil
	end

	def drag_and_drop_date
		@issues = Issue.where(:id => params[:issue_id])
		@issue = @issues.first

		@project = Project.find(@issue.project_id)
		@can_edit = User.current.allowed_to?(:edit_issues, @project)
		if @can_edit then
			date = Date.strptime(params[:date], "%Y-%m-%d")
			is_start = params[:is_start] != ""
			is_end = params[:is_end] != ""

			old_start_date = @issue.start_date
			old_due_date = @issue.due_date

			start_date = old_start_date
			due_date = old_due_date

			if is_start && is_end then
				if start_date && date < start_date then
					start_date = date
				end
				if due_date && date > due_date then
					due_date = date
				end
			elsif is_start then
				start_date = date
				if due_date && due_date < date then
					due_date = date
				end
			elsif is_end then
				due_date = date
				if start_date && start_date > date
					start_date = date
				end
			end

			if start_date != old_start_date || due_date != old_due_date then
				journal = @issue.init_journal(User.current, "Changed by 'standup' plugin.")
				@issue.safe_attributes = {
					"start_date" => start_date,
					"due_date" => due_date,
				}
				ok = @issue.save
			end

		end
	rescue
	ensure
		__prep_table
		render :action => "table", :layout => false
		nil
	end

	def new_issue
		@project = Project.find(params[:project_id])
		@can_add = User.current.allowed_to?(:add_issues, @project)
		assignee = params[:assignee]
		assignable = true # TODO: project.assignable_users.include? assignee
		if @can_add and assignable then
			@issue = Issue.create :project => @project,
				:author => User.current,
				:assigned_to_id => assignee,
				:tracker_id => params[:tracker_id],
				:category_id => params[:category_id],
				:parent_issue_id => params[:parent_id],
				:start_date => Date.today,
				:due_date => Date.today,
				:subject => params[:subject]
			#journal = @issue.init_journal(User.current, "Created by 'standup' plugin.")
		end
	rescue
	ensure
		__prep_table
		render :action => "table", :layout => false
		nil
	end

	def context_menu
		issue_id = params[:context_menu_issue_id]
		@issues = Issue.where(:id => issue_id)

		if ! @issues.all?(&:visible?) then
			@issues = []
		end

		render :layout => false
		nil
	rescue ActiveRecord::RecordNotFound
		render_404
		nil
	end
end

