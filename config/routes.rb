# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

get 'standup', :to => 'standup#index'
match 'standup/table', :to => 'standup#table', :via => [:get, :post]

post 'standup/set_status', :to => 'standup#set_status'
post 'standup/set_assignee', :to => 'standup#set_assignee'
post 'standup/set_done_ratio', :to => 'standup#set_done_ratio'
post 'standup/new_issue', :to => 'standup#new_issue'
post 'standup/drag_and_drop_date', :to => 'standup#drag_and_drop_date'

match 'standup/context_menu', :to => 'standup#context_menu', :as => 'standup_context_menu', :via => [:get, :post]

