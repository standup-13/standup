Redmine::Plugin.register :standup do
	name 'Standup plugin'
	author 'Author name'
	description 'This is a plugin for Redmine'
	version '0.0.1'
	url 'http://example.com/path/to/plugin'
	author_url 'http://example.com/about'

	menu(
		:top_menu,
		:standup_plugin,
		{
			controller: 'standup',
			action: 'index'
		},
		caption: "Standup",
	)
end
